#include <stdio.h>

int main()
{
    int i, n, smallest, largest, small_pos, large_pos, final_largest, final_smallest;
    printf("Enter the number of elements \n");
    scanf("%d",&n);
   int arr[n];
    printf(" Enter the elements \n");
    for(i=0; i<n; i++)
    {
        scanf("%d",&arr[i]);
    }
    
    // Finding smallest number & largest number
    
    smallest = arr[0];
     small_pos = 0;
     largest = arr[0];
    large_pos = 0;
    for(i=1; i<n; i++)
    {
        if(smallest > arr[i])
        {
            smallest = arr[i];
        
            small_pos = i;
        }
        
        if(arr[i]>largest)
        {
            largest = arr[i];
        
            large_pos = i;
        }
    }
    printf("The Smallest number of the array is %d \n", smallest);
    
    printf("The Largest Number of the array is %d \n", largest);
    
    
    // swapping
    
    final_smallest = largest;
    final_largest = smallest;
    printf("After Inter-changing the Largest & Smallest elements of the array \n");
    printf("The Largest element is %d \n", final_largest);
    printf("The Smallest element is %d \n", final_smallest);
    
    
    return 0;
    
}