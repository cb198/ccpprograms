int main()
{
   int a,b,c,d;
   double root1, root2;
   printf("Enter the Co-Efficient A of the Quadratic Equation  \n");
   scanf(" %d ", &a);
   printf("Enter the Co-Efficient B of the Quadratic Equation  \n");
   scanf(" %d ", &b);
   printf("Enter the Co-Efficient C of the Quadratic Equation  \n");
   scanf(" %d ", &c);
   printf(" The given Co-Efficients are  %d  %d  %d   \n " , a, b, c);
   d = (b*b) - 4*a*c ; 
   root1 = (-b + sqrt(d))/(2*a);
   root2 = (-b - sqrt(d))/(2*a);
   printf("First root= %.2lf  \n ", root1);
   printf("Second root= %.2lf \n ", root2);
   
}